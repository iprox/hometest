//
//  ProkofievTestTests.swift
//  ProkofievTestTests
//
//  Created by Oleksandr Prokofiev on 16/7/17.
//  Copyright © 2017 Oleksandr Prokofiev. All rights reserved.
//

import XCTest
@testable import ProkofievTest

class ProkofievTestTests: XCTestCase {

  func testMentions() {
    let testString = "@chris you around?"

    let expectation = self.expectation(description: "mentions expectation")
    do {
      try mentionTokenizer(testString) { result in
        XCTAssertEqual(result.keys.count, 1)
        let mentions = result["mentions"] as? [String]
        XCTAssertEqual(mentions?.count, 1)
        XCTAssertTrue(mentions!.contains("chris"))
        expectation.fulfill()
      }
    } catch {
      XCTFail()
      expectation.fulfill()
    }

    wait(for: [expectation], timeout: 1.0)
  }

  func testEmocions() {
    let testString = "Good morning! (megusta) (coffee)"

    let expectation = self.expectation(description: "emocions expectation")
    do {
      try emocionTokenizer(testString) { result in
        XCTAssertEqual(result.keys.count, 1)
        let emocions = result["emocions"] as? [String]
        XCTAssertEqual(emocions?.count, 2)
        XCTAssertTrue(emocions!.contains("megusta"))
        XCTAssertTrue(emocions!.contains("coffee"))
        expectation.fulfill()
      }
    } catch {
      XCTFail()
      expectation.fulfill()
    }

    wait(for: [expectation], timeout: 1.0)
  }


  func testLinks() {
    let testString = "Olympics are starting soon;http://www.nbcolympics.com"

    let expectation = self.expectation(description: "emocions expectation")
    do {
      try linkTokenizer(testString) { result in
        XCTAssertEqual(result.keys.count, 1)
        let links = result["links"] as? [[String: String]]
        XCTAssertNotNil(links)
        XCTAssertEqual(links?.count, 1)
        let olympicsDictionary = links?.first
        XCTAssertNotNil(olympicsDictionary)
        XCTAssertEqual(olympicsDictionary?["url"], "http://www.nbcolympics.com")
        XCTAssertEqual(olympicsDictionary?["title"], "NBC Olympics | 2014 NBC Olympics in Sochi Russia")
        expectation.fulfill()
      }
    } catch {
      XCTFail()
      expectation.fulfill()
    }

    wait(for: [expectation], timeout: 30.0)
  }

  func testComposedTokenizer() {
    let composedTokenizer: MessageTokenizer = compose([mentionTokenizer, emocionTokenizer, linkTokenizer])
    let testString = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
    let expectation = self.expectation(description: "Composed tokenizer expectation")
    do {
      try composedTokenizer(testString) { result in
        XCTAssertEqual(result.keys.count, 3)

        let links = result["links"] as? [[String: String]]
        XCTAssertNotNil(links)
        XCTAssertEqual(links?.count, 1)
        let twitterDictionary = links?.first
        XCTAssertNotNil(twitterDictionary)
        XCTAssertEqual(twitterDictionary?["url"], "https://twitter.com/jdorfman/status/430511497475670016")
        XCTAssertEqual(twitterDictionary?["title"], "Twitter / jdorfman: nice @littlebigdetail from ...")

        let emocions = result["emocions"] as? [String]
        XCTAssertEqual(emocions?.count, 1)
        XCTAssertEqual(emocions!.contains("success"), true)

        let mentions = result["mentions"] as? [String]
        XCTAssertEqual(mentions?.count, 2)
        XCTAssertTrue(mentions!.contains("bob"))
        XCTAssertTrue(mentions!.contains("john"))

        expectation.fulfill()
      }
    } catch {
      XCTFail()
      expectation.fulfill()
    }

    wait(for: [expectation], timeout: 30.0)
  }

}
