//
//  FoundationExtensions.swift
//  ProkofievTest
//
//  Created by Oleksandr Prokofiev on 16/7/17.
//  Copyright © 2017 Oleksandr Prokofiev. All rights reserved.
//

import Foundation

extension Dictionary {
  mutating func update(other: Dictionary) {
    for (key,value) in other {
      self.updateValue(value, forKey:key)
    }
  }
}
