//
//  PageRequest.swift
//  ProkofievTest
//
//  Created by Oleksandr Prokofiev on 16/7/17.
//  Copyright © 2017 Oleksandr Prokofiev. All rights reserved.
//

import Foundation

struct Network {
  static let session = URLSession(configuration: .default)
}

enum RequestResult<Model, Error> {
  case success(Model)
  case failure(Error)
}

enum PageRequestError: Error {
  case noData
}

class PageRequest {

  let url: URL

  init(url: URL) {
    self.url = url
  }

  func send(completion: @escaping (RequestResult<String, Error>) -> Void) {
    #if TESTING
      let title: String
      switch url.absoluteString {
      case "http://www.nbcolympics.com":
        title = "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
      case "https://twitter.com/jdorfman/status/430511497475670016":
        title = "Twitter / jdorfman: nice @littlebigdetail from ..."
      default:
        title = ""
      }
      let htmlString = "<html><head><title>\(title)</title></head><body>Some body data</body></html>"
      completion(.success(htmlString))
    #else
      Network.session.dataTask(with: url) { (data, response, error) in
        if let error = error {
          completion(.failure(error))
          return
        }

        if let data = data, let htmlString = String(data: data, encoding: .utf8) {
          completion(.success(htmlString))
        } else {
          completion(.failure(PageRequestError.noData))
        }
      }.resume()
    #endif
  }

}
