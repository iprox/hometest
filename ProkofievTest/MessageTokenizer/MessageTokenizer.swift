//
//  MessageTokenizer.swift
//  ProkofievTest
//
//  Created by Oleksandr Prokofiev on 16/7/17.
//  Copyright © 2017 Oleksandr Prokofiev. All rights reserved.
//

import Foundation
import Kanna

typealias MessageProccessingCompletion = (_ tokens: [String: [Any]]) -> Void
typealias MessageTokenizer = (_ message: String, _ processingCompletion: @escaping MessageProccessingCompletion) throws -> Void

func compose(_ tokenizers: [MessageTokenizer]) -> MessageTokenizer {
  return { message, completion in
    let group = DispatchGroup()
    var tokens: [String: [Any]] = [:]

    try tokenizers.forEach {
      group.enter()
      try $0(message) { token in
        group.leave()
        tokens.update(other: token)
      }
    }

    group.notify(queue: DispatchQueue.main) {
      completion(tokens)
    }
  }
}

func regexpTokenizer(with pattern: String, key: String) -> MessageTokenizer {
  return { message, completion in
    let nsString = message as NSString
    let regex = try NSRegularExpression(pattern: pattern, options: [])
    let matches = regex.matches(in: message, options: [], range: NSMakeRange(0, nsString.length)).map {
      nsString.substring(with: $0.range)
    }
    completion(matches.count > 0 ? [key: matches] : [:])
  }
}

let mentionTokenizer: MessageTokenizer = regexpTokenizer(with: "(?<=@)\\w+", key: "mentions")
let emocionTokenizer: MessageTokenizer = regexpTokenizer(with: "(?<=\\()[^()]{1,15}(?=\\))", key: "emocions")

let linkTokenizer: MessageTokenizer = { message, completion in
  let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
  let matches = detector.matches(in: message, options: [], range: NSRange(location: 0, length: message.utf16.count))

  let urls: [URL] = matches.flatMap { match in
    return URL(string: (message as NSString).substring(with: match.range))
  }

  let group = DispatchGroup()

  var links: [[String: String]] = []
  for url in urls {
    let request = PageRequest(url: url)
    group.enter()

    var linkData: [String: String] = ["url": String(describing: url)]
    request.send() { result in
      switch result {
      case .success(let htmlString):
        let doc = HTML(html: htmlString, encoding: .utf8)
        if let title = doc?.title {
          linkData["title"] = title
        }
      case .failure:
        break
      }
      links.append(linkData)
      group.leave()
    }
  }

  group.notify(queue: DispatchQueue.main) {
    completion(links.count > 0 ? ["links": links] : [:])
  }

}

