//
//  HTMLParser.swift
//  ProkofievTest
//
//  Created by Oleksandr Prokofiev on 16/7/17.
//  Copyright © 2017 Oleksandr Prokofiev. All rights reserved.
//

import Foundation

// clearly this solution does not work because HTML is not valid XML

final class HTMLValueExtractor: NSObject {

  fileprivate let tag: String
  fileprivate let xmlParser: XMLParser
  fileprivate var foundCharacters: String = ""
  fileprivate var completion: ((String?) -> Void)?

  init(htmlData: Data, tag: String) {
    self.tag = tag.lowercased()
    self.xmlParser = XMLParser(data: htmlData)
    super.init()
    self.xmlParser.delegate = self
  }

  func parse(completion: @escaping (String?) -> Void) {
    self.completion = completion
    self.xmlParser.parse()
  }

  func notify(with value: String?) {
    self.completion?(value)
    self.completion = nil
  }
}

extension HTMLValueExtractor: XMLParserDelegate {

  func parser(_ parser: XMLParser, foundCharacters string: String) {
    self.foundCharacters += string
  }

  func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
    if elementName.lowercased() == tag {
      parser.abortParsing()
      notify(with: foundCharacters)
    }
    self.foundCharacters = ""
  }

  func parserDidEndDocument(_ parser: XMLParser) {
    self.notify(with: nil)
  }

  func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
    self.notify(with: nil)
  }

}
