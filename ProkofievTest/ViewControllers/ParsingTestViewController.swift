//
//  ParsingTestViewController.swift
//  ProkofievTest
//
//  Created by Oleksandr Prokofiev on 16/7/17.
//  Copyright © 2017 Oleksandr Prokofiev. All rights reserved.
//

import UIKit

protocol ParsingTestViewProtocol: class {
  func setLoading(_ enabled: Bool)
  func setDefaultText(_ text: String)
  func updateJsonLabel(with text: String)
  func presentError(_ error: Error)
}

class ParsingTestPresenter {

  weak var view: ParsingTestViewProtocol? = nil
  private var text = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"

  private var isLoading: Bool = false {
    didSet {
      view?.setLoading(isLoading)
    }
  }

  func onViewReady() {
    view?.setDefaultText(text)
  }

  func didUpdateText(_ text: String) {
    self.text = text
  }

  func didTapTokenize() {
    isLoading = true

    let tokenizer = compose([mentionTokenizer, emocionTokenizer, linkTokenizer])
    do {
      try tokenizer(text) { [weak self] result in
        let data = try! JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
        let jsonString = String.init(data: data, encoding: .utf8) ?? ""
        self?.view?.updateJsonLabel(with: jsonString)
        self?.isLoading = false
      }
    } catch {
      isLoading = false
      view?.presentError(error)
    }
  }

}

class ParsingTestViewController: UIViewController {

  @IBOutlet weak var jsonTextView: UITextView!
  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var actionButton: UIButton!

  var presenter = ParsingTestPresenter()

  override func viewDidLoad() {
    super.viewDidLoad()
    presenter.view = self
    presenter.onViewReady()
    view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapRecognizer)))
  }

  @IBAction func didTapTokenize(_ sender: Any) {
    presenter.didTapTokenize()
  }

  func onTapRecognizer() {
    textView.resignFirstResponder()
  }
}

extension ParsingTestViewController: ParsingTestViewProtocol {

  func setDefaultText(_ text: String) {
    self.textView.text = text
  }

  func setLoading(_ enabled: Bool) {
    textView.isEditable = !enabled
    actionButton.isHidden = enabled
    if enabled {
      activityIndicator.startAnimating()
    } else {
      activityIndicator.stopAnimating()
    }
  }

  func updateJsonLabel(with text: String) {
    jsonTextView.text = text
  }

  func presentError(_ error: Error) {
    let alert = UIAlertController(title: "Error",
                                  message: error.localizedDescription,
                                  preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    present(alert, animated: true, completion: nil)
  }
}

extension ParsingTestViewController: UITextViewDelegate {
  func textViewDidChange(_ textView: UITextView) {
    presenter.didUpdateText(textView.text)
  }
}
